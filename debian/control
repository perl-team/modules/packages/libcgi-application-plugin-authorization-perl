Source: libcgi-application-plugin-authorization-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Build-Depends-Indep: libapache-htgroup-perl <!nocheck>,
                     libcgi-application-basic-plugin-bundle-perl <!nocheck>,
                     libcgi-application-perl <!nocheck>,
                     libcgi-pm-perl <!nocheck>,
                     libdbd-sqlite3-perl <!nocheck>,
                     libnet-ldap-perl <!nocheck>,
                     libtest-exception-perl <!nocheck>,
                     libtest-pod-coverage-perl <!nocheck>,
                     libtest-pod-perl <!nocheck>,
                     libtest-warn-perl <!nocheck>,
                     libuniversal-require-perl <!nocheck>,
                     perl
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libcgi-application-plugin-authorization-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libcgi-application-plugin-authorization-perl.git
Homepage: https://metacpan.org/release/CGI-Application-Plugin-Authorization
Rules-Requires-Root: no

Package: libcgi-application-plugin-authorization-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libcgi-application-basic-plugin-bundle-perl,
         libcgi-application-perl,
         libuniversal-require-perl
Recommends: libapache-htgroup-perl,
            libcgi-application-plugin-authentication-perl,
            libnet-ldap-perl
Enhances: libcgi-application-perl
Provides: libcgi-application-plugin-authorization-driver-activedirectory-perl
Description: authorization framework for CGI::Application
 CGI::Application::Plugin::Authorization adds the ability to authorize users
 for specific tasks. Once a user has been authenticated and you know who you
 are dealing with, you can then use this plugin to control what that user has
 access to. It imports two methods (authz and authorization) into your
 CGI::Application module.  Both of these methods are interchangeable, so you
 should choose one and use it consistently throughout your code.  Through the
 authz method you can call all the methods of the
 CGI::Application::Plugin::Authorization plugin.
 .
 This package also bundles an active directory driver:
 CGI::Application::Plugin::Authorization::Driver::ActiveDirectory
